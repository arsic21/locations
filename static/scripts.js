// Langauage swap
function changeLanguage(clicked, active) {
  clicked.classList.add("active-lang");
  active.classList.remove("active-lang");
}

let srbLang = document.querySelector("#srb");
let engLang = document.querySelector("#eng");

srbLang.addEventListener("click", (e) => {
  e.preventDefault();
  changeLanguage(srbLang, engLang);
});

engLang.addEventListener("click", (e) => {
  e.preventDefault();
  changeLanguage(engLang, srbLang);
});

// show nav

function showNav() {
  if (!isNavActive) {
    navCollapse.classList.add("show");
    togglerButton.classList.add("open");
    isNavActive = true;
  } else {
    navCollapse.classList.remove("show");
    togglerButton.classList.remove("open");
    isNavActive = false;
  }
}

let navCollapse = document.querySelector("#nav-collapse");
let isNavActive = false;
let togglerButton = document.querySelector("#toggler-button");

togglerButton.addEventListener("click", (e) => {
  showNav();
});

// nav menus

function openMenu(clicked) {
  if (clicked.classList.contains("open")) {
    clicked.classList.remove("open");
    clicked.nextElementSibling.classList.remove("open");
  } else {
    for (let menu of dropDowns) {
      menu.classList.remove("open");
    }
    for (let button of listButtons) {
      button.classList.remove("open");
    }
    clicked.classList.add("open");
    clicked.nextElementSibling.classList.add("open");
  }
}

let listButtons = document.querySelectorAll(".list-button");
let dropDowns = document.querySelectorAll(".drop-down-menu");

for (let button of listButtons) {
  button.addEventListener("click", () => {
    openMenu(button);
  });
}

//open close modal

function openModal() {
  modal.classList.add("open");
}
function closeModal() {
  modal.classList.remove("open");
}

let openButton = document.querySelector("#cart");
let closeButton = document.querySelector("#modal-button");
let modal = document.querySelector("#modal");

openButton.addEventListener("click", (e) => {
  e.preventDefault();
  openModal();
});

document.addEventListener("click", (e) => {
  e.preventDefault();
  if (e.target == closeButton || e.target.parentNode == closeButton || e.target == modal) {
    closeModal();
  }
});
